package com.nerv.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nerv.exeption.ModeloNotFoundException;
import com.nerv.model.Examen;
import com.nerv.service.intr.IExamenService;

/**
 * Clase que se define como un Controlador y contiene la definición de los
 * servicios RestFull para la entidad Examen.
 * 
 * @author santos
 *
 */
@RestController
@RequestMapping("/examenes")
public class ExamenController {

    @Autowired
    private IExamenService serviceExamen;

    /*
     * NOTA: Se recomienda englobal las respustas en la clase (ResponseEntity) para
     * poder manipular la petición HTTP
     */

    /**
     * Servicio RestFull que permite listar todos regsitros de los Examenes.
     * 
     * @return Examenes Lista de Examenes.
     */
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Examen>> listar() {
        List<Examen> examenes = new ArrayList<>();
        examenes = serviceExamen.listar();
        return new ResponseEntity<List<Examen>>(examenes, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite listar el registro del Examen que tiene
     * asociado el id enviado como parametro.
     * 
     * @param id Código (llave) del registro.
     * @return Examen Lista el Examen.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Examen> listarId(@PathVariable("id") Integer id) {
        Examen examen = serviceExamen.listarId(id);
        if (examen == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        }
        return new ResponseEntity<Examen>(examen, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite registrar un nuevo registro de Examen.
     * 
     * @param examen Examen a registrar.
     * @return Examen Examen registrado.
     */
    @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Examen> registrar(@RequestBody Examen examen) {
        Examen examenRegistrado = new Examen();
        examenRegistrado = serviceExamen.registrar(examen);
        // Cuando se crea un recurso, las buenas practicas dicen que se debe exponer la
        // URL del recurso creado.
        URI localizacionNuevoRecurso = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(examenRegistrado.getIdExamen()).toUri();
        // Cuando se crea un recurso, no se debe mostrar (200) si no un (201) - CREATED
        return ResponseEntity.created(localizacionNuevoRecurso).build();
    }

    /**
     * Servicio RestFull que permite modificar un registro de un Examen.
     * 
     * @param examen Examen a modificar.
     * @return Examen Examen modifico.
     */
    @PutMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Examen> modificar(@RequestBody Examen examen) {
        serviceExamen.registrar(examen);
        return new ResponseEntity<Examen>(HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite eliminar un Examen.
     * 
     * @param id Código (llave) del registro.
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void eliminar(@PathVariable("id") Integer id) {
        Examen examen = serviceExamen.listarId(id);
        if (examen == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            serviceExamen.eliminar(id);
        }
    }
}
