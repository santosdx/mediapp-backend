package com.nerv.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nerv.exeption.ModeloNotFoundException;
import com.nerv.model.Medico;
import com.nerv.service.intr.IMedicoService;

/**
 * Clase que se define como un Controlador y contiene la definición de los
 * servicios RestFull para la entidad Medico.
 * 
 * @author santos
 *
 */
@RestController
@RequestMapping("/medicos")
public class MedicoController {

    @Autowired
    private IMedicoService serviceMedico;

    /*
     * NOTA: Se recomienda englobal las respustas en la clase (ResponseEntity) para
     * poder manipular la petición HTTP
     */

    /**
     * Servicio RestFull que permite listar todos regsitros de los Medicos.
     * 
     * @return Medicos Lista de Medicos.
     */
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Medico>> listar() {
        List<Medico> medicos = new ArrayList<>();
        medicos = serviceMedico.listar();
        return new ResponseEntity<List<Medico>>(medicos, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite listar el registro del Medico que tiene
     * asociado el id enviado como parametro.
     * 
     * @param id Código (llave) del registro.
     * @return Medico Lista el Medico.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Medico> listarId(@PathVariable("id") Integer id) {
        Medico medico = serviceMedico.listarId(id);
        if (medico == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        }
        return new ResponseEntity<Medico>(medico, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite registrar un nuevo registro de Medico.
     * 
     * @param medico Medico a registrar.
     * @return Medico Medico registrado.
     */
    @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Medico> registrar(@RequestBody Medico medico) {
        Medico medicoRegistrado = new Medico();
        medicoRegistrado = serviceMedico.registrar(medico);
        // Cuando se crea un recurso, las buenas practicas dicen que se debe exponer la
        // URL del recurso creado.
        URI localizacionNuevoRecurso = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(medicoRegistrado.getIdMedico()).toUri();
        // Cuando se crea un recurso, no se debe mostrar (200) si no un (201) - CREATED
        return ResponseEntity.created(localizacionNuevoRecurso).build();
    }

    /**
     * Servicio RestFull que permite modificar un registro de un Medico.
     * 
     * @param medico Medico a modificar.
     * @return Medico Medico modifico.
     */
    @PutMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Medico> modificar(@RequestBody Medico medico) {
        serviceMedico.registrar(medico);
        return new ResponseEntity<Medico>(HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite eliminar un Medico.
     * 
     * @param id Código (llave) del registro.
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void eliminar(@PathVariable("id") Integer id) {
        Medico medico = serviceMedico.listarId(id);
        if (medico == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            serviceMedico.eliminar(id);
        }
    }
}
