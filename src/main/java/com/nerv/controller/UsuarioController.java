package com.nerv.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nerv.exeption.ModeloNotFoundException;
import com.nerv.model.Usuario;
import com.nerv.service.intr.IUsuarioService;

/**
 * Clase que se define como un Controlador y contiene la definición de los
 * servicios RestFull para la entidad Usuario.
 * 
 * @author santos
 *
 */
@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private IUsuarioService serviceUsuario;

    /*
     * NOTA: Se recomienda englobal las respustas en la clase (ResponseEntity) para
     * poder manipular la petición HTTP
     */

    /**
     * Servicio RestFull que permite listar todos regsitros de los Usuarios.
     * 
     * @return Usuarios Lista de Usuarios.
     */
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Usuario>> listar() {
        List<Usuario> usuarios = new ArrayList<>();
        usuarios = serviceUsuario.listar();
        return new ResponseEntity<List<Usuario>>(usuarios, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite listar el registro del Usuario que tiene
     * asociado el id enviado como parametro.
     * 
     * @param id Código (llave) del registro.
     * @return Usuario Lista el Usuario.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Usuario> listarId(@PathVariable("id") Integer id) {
        Usuario usuario = serviceUsuario.listarId(id);
        if (usuario == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        }
        return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite registrar un nuevo registro de Usuario.
     * 
     * @param usuario Usuario a registrar.
     * @return Usuario Usuario registrado.
     */
    @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Usuario> registrar(@RequestBody Usuario usuario) {
        Usuario usuarioRegistrado = new Usuario();
        usuarioRegistrado = serviceUsuario.registrar(usuario);
        // Cuando se crea un recurso, las buenas practicas dicen que se debe exponer la
        // URL del recurso creado.
        URI localizacionNuevoRecurso = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(usuarioRegistrado.getIdUsuario()).toUri();
        // Cuando se crea un recurso, no se debe mostrar (200) si no un (201) - CREATED
        return ResponseEntity.created(localizacionNuevoRecurso).build();
    }

    /**
     * Servicio RestFull que permite modificar un registro de un Usuario.
     * 
     * @param usuario Usuario a modificar.
     * @return Usuario Usuario modifico.
     */
    @PutMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Usuario> modificar(@RequestBody Usuario usuario) {
        serviceUsuario.registrar(usuario);
        return new ResponseEntity<Usuario>(HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite eliminar un Usuario.
     * 
     * @param id Código (llave) del registro.
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void eliminar(@PathVariable("id") Integer id) {
        Usuario usuario = serviceUsuario.listarId(id);
        if (usuario == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            serviceUsuario.eliminar(id);
        }
    }
}
