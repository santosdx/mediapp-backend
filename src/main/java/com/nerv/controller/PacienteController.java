package com.nerv.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nerv.exeption.ModeloNotFoundException;
import com.nerv.model.Paciente;
import com.nerv.service.intr.IPacienteService;

/**
 * Clase que se define como un Controlador y contiene la definición de los
 * servicios RestFull para la entidad Paciente.
 * 
 * @author santos
 *
 */
@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    @Autowired
    private IPacienteService servicePaciente;

    /*
     * NOTA: Se recomienda englobal las respustas en la clase (ResponseEntity) para
     * poder manipular la petición HTTP
     */

    /**
     * Servicio RestFull que permite listar todos regsitros de los Pacientes.
     * 
     * @return Pacientes Lista de Pacientes.
     */
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Paciente>> listar() {
        List<Paciente> pacientes = new ArrayList<>();
        pacientes = servicePaciente.listar();
        return new ResponseEntity<List<Paciente>>(pacientes, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite listar el registro del Paciente que tiene
     * asociado el id enviado como parametro.
     * 
     * @param id Código (llave) del registro.
     * @return Paciente Lista el Paciente.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Paciente> listarId(@PathVariable("id") Integer id) {
        Paciente paciente = servicePaciente.listarId(id);
        if (paciente == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        }
        return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite registrar un nuevo registro de Paciente.
     * 
     * @param paciente Paciente a registrar.
     * @return Paciente Paciente registrado.
     */
    @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Paciente> registrar(@RequestBody Paciente paciente) {
        Paciente pacienteRegistrado = new Paciente();
        pacienteRegistrado = servicePaciente.registrar(paciente);
        // Cuando se crea un recurso, las buenas practicas dicen que se debe exponer la
        // URL del recurso creado.
        URI localizacionNuevoRecurso = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(pacienteRegistrado.getIdPaciente()).toUri();
        // Cuando se crea un recurso, no se debe mostrar (200) si no un (201) - CREATED
        return ResponseEntity.created(localizacionNuevoRecurso).build();
    }

    /**
     * Servicio RestFull que permite modificar un registro de un Paciente.
     * 
     * @param paciente Paciente a modificar.
     * @return Paciente Paciente modificado.
     */
    @PutMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Paciente> modificar(@RequestBody Paciente paciente) {
        servicePaciente.registrar(paciente);
        return new ResponseEntity<Paciente>(HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite eliminar un Paciente.
     * 
     * @param id Código (llave) del registro.
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void eliminar(@PathVariable("id") Integer id) {
        Paciente paciente = servicePaciente.listarId(id);
        if (paciente == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            servicePaciente.eliminar(id);
        }
    }
}
