package com.nerv.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nerv.exeption.ModeloNotFoundException;
import com.nerv.model.Menu;
import com.nerv.service.intr.IMenuService;

/**
 * Clase que se define como un Controlador y contiene la definición de los
 * servicios RestFull para la entidad Menu.
 * 
 * @author santos
 *
 */
@RestController
@RequestMapping("/menus")
public class MenuController {

    @Autowired
    private IMenuService serviceMenu;

    /*
     * NOTA: Se recomienda englobal las respustas en la clase (ResponseEntity) para
     * poder manipular la petición HTTP
     */

    /**
     * Servicio RestFull que permite listar todos regsitros de los Menus.
     * 
     * @return Menus Lista de Menus.
     */
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Menu>> listar() {
        List<Menu> menus = new ArrayList<>();
        menus = serviceMenu.listar();
        return new ResponseEntity<List<Menu>>(menus, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite listar el registro del Menu que tiene asociado
     * el id enviado como parametro.
     * 
     * @param id Código (llave) del registro.
     * @return Menu Lista el Menu.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Menu> listarId(@PathVariable("id") Integer id) {
        Menu menu = serviceMenu.listarId(id);
        if (menu == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        }
        return new ResponseEntity<Menu>(menu, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite registrar un nuevo registro de Menu.
     * 
     * @param menu Menu a registrar.
     * @return Menu Menu registrado.
     */
    @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Menu> registrar(@RequestBody Menu menu) {
        Menu menuRegistrado = new Menu();
        menuRegistrado = serviceMenu.registrar(menu);
        // Cuando se crea un recurso, las buenas practicas dicen que se debe exponer la
        // URL del recurso creado.
        URI localizacionNuevoRecurso = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(menuRegistrado.getIdMenu()).toUri();
        // Cuando se crea un recurso, no se debe mostrar (200) si no un (201) - CREATED
        return ResponseEntity.created(localizacionNuevoRecurso).build();
    }

    /**
     * Servicio RestFull que permite modificar un registro de un Menu.
     * 
     * @param menu Menu a modificar.
     * @return Menu Menu modificado.
     */
    @PutMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Menu> modificar(@RequestBody Menu menu) {
        serviceMenu.registrar(menu);
        return new ResponseEntity<Menu>(HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite eliminar un Menu.
     * 
     * @param id Código (llave) del registro.
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void eliminar(@PathVariable("id") Integer id) {
        Menu menu = serviceMenu.listarId(id);
        if (menu == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            serviceMenu.eliminar(id);
        }
    }
}
