package com.nerv.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nerv.exeption.ModeloNotFoundException;
import com.nerv.model.Especialidad;
import com.nerv.service.intr.IEspecialidadService;

/**
 * Clase que se define como un Controlador y contiene la definición de los
 * servicios RestFull para la entidad Especialidad.
 * 
 * @author santos
 *
 */
@RestController
@RequestMapping("/especialidades")
public class EspecialidadController {

    @Autowired
    private IEspecialidadService serviceEspecialidad;

    /*
     * NOTA: Se recomienda englobal las respustas en la clase (ResponseEntity) para
     * poder manipular la petición HTTP
     */

    /**
     * Servicio RestFull que permite listar todos regsitros de las Especialidades.
     * 
     * @return Especialidades Lista de Especialidades.
     */
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Especialidad>> listar() {
        List<Especialidad> especialidad = new ArrayList<>();
        especialidad = serviceEspecialidad.listar();
        return new ResponseEntity<List<Especialidad>>(especialidad, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite listar el registro de la Especialidad que tiene
     * asociado el id enviado como parametro.
     * 
     * @param id Código (llave) del registro.
     * @return Especialidad Lista la Especialidad.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Especialidad> listarId(@PathVariable("id") Integer id) {
        Especialidad especialidad = serviceEspecialidad.listarId(id);
        if (especialidad == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        }
        return new ResponseEntity<Especialidad>(especialidad, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite registrar un nuevo registro de Especialidad.
     * 
     * @param medico Especialidad a registrar.
     * @return Especialidad Especialidad registrada.
     */
    @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Especialidad> registrar(@RequestBody Especialidad especialidad) {
        Especialidad especialidadRegistrada = new Especialidad();
        especialidadRegistrada = serviceEspecialidad.registrar(especialidad);
        // Cuando se crea un recurso, las buenas practicas dicen que se debe exponer la
        // URL del recurso creado.
        URI localizacionNuevoRecurso = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(especialidadRegistrada.getIdEspecialidad()).toUri();
        // Cuando se crea un recurso, no se debe mostrar (200) si no un (201) - CREATED
        return ResponseEntity.created(localizacionNuevoRecurso).build();
    }

    /**
     * Servicio RestFull que permite modificar un registro de una Especialidad.
     * 
     * @param especialidad Especialidad a modificar.
     * @return Especialidad Especialidad modificada.
     */
    @PutMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Especialidad> modificar(@RequestBody Especialidad especialidad) {
        serviceEspecialidad.registrar(especialidad);
        return new ResponseEntity<Especialidad>(HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite eliminar un Especialidad.
     * 
     * @param id Código (llave) del registro.
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void eliminar(@PathVariable("id") Integer id) {
        Especialidad especialidad = serviceEspecialidad.listarId(id);
        if (especialidad == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            serviceEspecialidad.eliminar(id);
        }
    }
}
