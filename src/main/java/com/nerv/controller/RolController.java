package com.nerv.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nerv.exeption.ModeloNotFoundException;
import com.nerv.model.Rol;
import com.nerv.service.intr.IRolService;

/**
 * Clase que se define como un Controlador y contiene la definición de los
 * servicios RestFull para la entidad Rol.
 * 
 * @author santos
 *
 */
@RestController
@RequestMapping("/roles")
public class RolController {

    @Autowired
    private IRolService serviceRol;

    /*
     * NOTA: Se recomienda englobal las respustas en la clase (ResponseEntity) para
     * poder manipular la petición HTTP
     */

    /**
     * Servicio RestFull que permite listar todos regsitros de los Roles.
     * 
     * @return Roles Lista de Roles.
     */
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Rol>> listar() {
        List<Rol> roles = new ArrayList<>();
        roles = serviceRol.listar();
        return new ResponseEntity<List<Rol>>(roles, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite listar el registro del Rol que tiene asociado
     * el id enviado como parametro.
     * 
     * @param id Código (llave) del registro.
     * @return Rol Lista el Rol.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Rol> listarId(@PathVariable("id") Integer id) {
        Rol rol = serviceRol.listarId(id);
        if (rol == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        }
        return new ResponseEntity<Rol>(rol, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite registrar un nuevo registro de Rol.
     * 
     * @param rol Rol a registrar.
     * @return Rol Rol registrado.
     */
    @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Rol> registrar(@RequestBody Rol rol) {
        Rol rolRegistrado = new Rol();
        rolRegistrado = serviceRol.registrar(rol);
        // Cuando se crea un recurso, las buenas practicas dicen que se debe exponer la
        // URL del recurso creado.
        URI localizacionNuevoRecurso = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(rolRegistrado.getIdRol()).toUri();
        // Cuando se crea un recurso, no se debe mostrar (200) si no un (201) - CREATED
        return ResponseEntity.created(localizacionNuevoRecurso).build();
    }

    /**
     * Servicio RestFull que permite modificar un registro de un Rol.
     * 
     * @param rol Rol a modificar.
     * @return Rol Rol modificado.
     */
    @PutMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Rol> modificar(@RequestBody Rol rol) {
        serviceRol.registrar(rol);
        return new ResponseEntity<Rol>(HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite eliminar un Rol.
     * 
     * @param id Código (llave) del registro.
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void eliminar(@PathVariable("id") Integer id) {
        Rol rol = serviceRol.listarId(id);
        if (rol == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            serviceRol.eliminar(id);
        }
    }
}
