package com.nerv.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nerv.dto.ConsultaListaExamenDTO;
import com.nerv.exeption.ModeloNotFoundException;
import com.nerv.model.Consulta;
import com.nerv.service.intr.IConsultaService;

/**
 * Clase que se define como un Controlador y contiene la definición de los
 * servicios RestFull para la entidad Consulta.
 * 
 * @author santos
 *
 */
@RestController
@RequestMapping("/consultas")
public class ConsultaController {

    @Autowired
    private IConsultaService serviceConsulta;

    /*
     * NOTA: Se recomienda englobal las respustas en la clase (ResponseEntity) para
     * poder manipular la petición HTTP
     */

    /**
     * Servicio RestFull que permite listar todos regsitros de las Consultas.
     * 
     * @return Consultas Lista de Consultas.
     */
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Consulta>> listar() {
        List<Consulta> consulta = new ArrayList<>();
        consulta = serviceConsulta.listar();
        return new ResponseEntity<List<Consulta>>(consulta, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite listar el registro de la Consulta que tiene
     * asociado el id enviado como parametro.
     * 
     * @param id Código (llave) del registro.
     * @return Consulta Lista la Consulta.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Consulta> listarId(@PathVariable("id") Integer id) {
        Consulta consulta = serviceConsulta.listarId(id);
        if (consulta == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        }
        return new ResponseEntity<Consulta>(consulta, HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite registrar un nuevo registro de Consulta.
     * 
     * @param consulta Consulta a registrar.
     * @return Consulta Consulta registrada.
     * @deprecated Esta consulta es mejorada por (registrarMaestroDetalleMasExamenesTransaccional)
     */
    // @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Consulta> registrar(@RequestBody Consulta consulta) {
        Consulta consultaRegistrada = new Consulta();
        consultaRegistrada = serviceConsulta.registrar(consulta);
        // Cuando se crea un recurso, las buenas practicas dicen que se debe exponer la
        // URL del recurso creado.
        URI localizacionNuevoRecurso = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(consultaRegistrada.getIdConsulta()).toUri();
        // Cuando se crea un recurso, no se debe mostrar (200) si no un (201) - CREATED
        return ResponseEntity.created(localizacionNuevoRecurso).build();
    }

    /**
     * Servicio RestFull que permite registrar un nuevo registro de Consulta Maestro
     * Detalle con la lista de Examenes.
     * 
     * @param consultaDetalleExamenDTO Consulta a registrar, de tipo maestro detalle
     *                                 con examenes.
     * @return Consulta Consulta registrada.
     */
    @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Consulta> registrarMaestroDetalleMasExamenesTransaccional(
            @RequestBody ConsultaListaExamenDTO consultaDetalleExamenDTO) {
        Consulta consultaRegistrada = new Consulta();
        consultaRegistrada = serviceConsulta.registrarMaestroDetalleMasExamenesTransaccional(consultaDetalleExamenDTO);
        return new ResponseEntity<Consulta>(consultaRegistrada, HttpStatus.CREATED);
    }

    /**
     * Servicio RestFull que permite modificar un registro de una Consulta.
     * 
     * @param consulta Consulta a modificar.
     * @return Consulta Consulta modificada.
     */
    @PutMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<Consulta> modificar(@RequestBody Consulta consulta) {
        serviceConsulta.registrar(consulta);
        return new ResponseEntity<Consulta>(HttpStatus.OK);
    }

    /**
     * Servicio RestFull que permite eliminar un Consulta.
     * 
     * @param id Código (llave) del registro.
     */
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void eliminar(@PathVariable("id") Integer id) {
        Consulta consulta = serviceConsulta.listarId(id);
        if (consulta == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            serviceConsulta.eliminar(id);
        }
    }
}
