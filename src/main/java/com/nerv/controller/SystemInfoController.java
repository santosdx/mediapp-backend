package com.nerv.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nerv.model.SystemInfo;
import com.nerv.service.intr.ISystemInfoService;

/**
 * Clase que se define como un Controller, y que contiene la definición de los
 * servicios RestFull para la entidad SystemInfo.
 * 
 * @author santos
 *
 */
@RestController
@RequestMapping("/systems")
public class SystemInfoController {

    @Autowired
    private ISystemInfoService serviceSystemInf;

    @GetMapping(produces = "application/json")
    public List<SystemInfo> listar() {
        return serviceSystemInf.listar();
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public SystemInfo listarId(@PathVariable("id") Integer id) {
        return serviceSystemInf.listarId(id);
    }

    @PostMapping(produces = "application/json", consumes = "application/json")
    public SystemInfo registrar(@RequestBody SystemInfo systemInfo) {
        return serviceSystemInf.registrar(systemInfo);
    }

    @PutMapping(produces = "application/json", consumes = "application/json")
    public SystemInfo modificar(@RequestBody SystemInfo systemInfo) {
        return serviceSystemInf.registrar(systemInfo);
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    public void eliminar(@PathVariable("id") Integer id) {
        serviceSystemInf.eliminar(id);
    }
}
