package com.nerv.dto;

import java.util.List;

import com.nerv.model.Consulta;
import com.nerv.model.Examen;

/**
 * Clase DTO que representa un objeto de Detalle Maestro, con la estructura de
 * una consulta, sus atributos y la lista de examenes. Esto para realizar la
 * insercción de la consulta y el detalle que es obligatorio, pero a demas la
 * lista de examenes que no es obligatoria.
 * 
 * @author santos
 *
 */
public class ConsultaListaExamenDTO {

    private Consulta consulta;
    private List<Examen> listaExamen;

    public Consulta getConsulta() {
        return consulta;
    }

    public void setConsulta(Consulta consulta) {
        this.consulta = consulta;
    }

    public List<Examen> getListaExamen() {
        return listaExamen;
    }

    public void setListaExamen(List<Examen> listaExamen) {
        this.listaExamen = listaExamen;
    }

}