package com.nerv.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nerv.model.Paciente;

/**
 * Interface que extiende de "JpaRepository", y que permite el uso del core de
 * JPA para el acceso a datos de la entidad Paciente.
 * 
 * @author santos
 *
 */
public interface IPacienteDAO extends JpaRepository<Paciente, Integer> {

    
    List<Paciente> findAllByOrderByNombresAsc();
}
