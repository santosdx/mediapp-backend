package com.nerv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nerv.model.Medico;

/**
 * Interface que extiende de "JpaRepository", y que permite el uso del core de
 * JPA para el acceso a datos de la entidad Medico.
 * 
 * @author santos
 *
 */
public interface IMedicoDAO extends JpaRepository<Medico, Integer> {

}
