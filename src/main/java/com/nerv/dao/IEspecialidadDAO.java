package com.nerv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nerv.model.Especialidad;

/**
 * Interface que extiende de "JpaRepository", y que permite el uso del core de
 * JPA para el acceso a datos de la entidad Especialidad.
 * 
 * @author santos
 *
 */
public interface IEspecialidadDAO extends JpaRepository<Especialidad, Integer> {

}
