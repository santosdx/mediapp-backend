package com.nerv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nerv.model.SystemInfo;

/**
 * Interface que extiende de "JpaRepository", y que permite el uso del core de
 * JPA para el acceso a datos de la entidad SystemInfo.
 * 
 * @author santos
 *
 */
public interface ISystemInfoDAO extends JpaRepository<SystemInfo, Integer>{

}
