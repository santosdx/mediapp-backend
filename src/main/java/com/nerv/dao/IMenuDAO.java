package com.nerv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nerv.model.Menu;

/**
 * Interface que extiende de "JpaRepository", y que permite el uso del core de
 * JPA para el acceso a datos de la entidad Menu.
 * 
 * @author santos
 *
 */
public interface IMenuDAO extends JpaRepository<Menu, Integer> {

}
