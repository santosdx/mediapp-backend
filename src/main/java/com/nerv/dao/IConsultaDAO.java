package com.nerv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nerv.model.Consulta;

/**
 * Interface que extiende de "JpaRepository", y que permite el uso del core de
 * JPA para el acceso a datos de la entidad Consulta.
 * 
 * @author santos
 *
 */
public interface IConsultaDAO extends JpaRepository<Consulta, Integer> {

}
