package com.nerv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nerv.model.Rol;

/**
 * Interface que extiende de "JpaRepository", y que permite el uso del core de
 * JPA para el acceso a datos de la entidad Rol.
 * 
 * @author santos
 *
 */
public interface IRolDAO extends JpaRepository<Rol, Integer> {

}
