/**
 * Paquete que contiene el listado de interfaces que interactuan con la base de datos mediante JPA, para implementar todas funciones.
 *
 */
/**
 * @author santos
 */
package com.nerv.dao;