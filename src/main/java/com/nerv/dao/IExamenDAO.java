package com.nerv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nerv.model.Examen;

/**
 * Interface que extiende de "JpaRepository", y que permite el uso del core de
 * JPA para el acceso a datos de la entidad Examen.
 * 
 * @author santos
 *
 */
public interface IExamenDAO extends JpaRepository<Examen, Integer> {

}
