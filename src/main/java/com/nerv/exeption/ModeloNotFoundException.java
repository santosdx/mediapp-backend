package com.nerv.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Clase que extiende de "RuntimeException" y para indicar que el Recurso no
 * encontrado. Se utiliza cuando el servidor web no encuentra la página o
 * recurso solicitado. 404 Not Found.
 * 
 * @author santos
 *
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ModeloNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Se utiliza cuando el servidor web no encuentra la página o recurso
     * solicitado. 404 Not Found.
     * 
     * @param mensaje Mensaje de error personalizado a mostrar.
     */
    public ModeloNotFoundException(String mensaje) {
        super(mensaje);
    }
}
