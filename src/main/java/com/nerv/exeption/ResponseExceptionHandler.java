package com.nerv.exeption;

import java.util.Date;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Clase que permite manipular y manejar todas las exceciones de respuesta de
 * los servicios del proyecto. Y devuelve los errores en formato JSON.
 * 
 * @author santos
 *
 */
@ControllerAdvice
@RestController
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Función que permite manipular cualquier otro tipo de excepción, por que la
     * manipula la clase padre (Exception).
     * 
     * @param ex      De tipo Exception
     * @param request Respuesta del servicio
     * @return ResponseEntity
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> manejarTodasExcepciones(Exception ex, WebRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
                request.getDescription(false));
        return new ResponseEntity<Object>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Función que valida la execepción "NOT FOUND", partiendo de la excención
     * personalizada "ModeloNotFoundException" y que responde el mensaje de error
     * con la extructura definida en la clase "ExceptionResponse".
     * 
     * @param ex      De tipo ModeloNotFoundException
     * @param request Respuesta del servicio
     * @return ResponseEntity Dettale del error generado
     */
    @ExceptionHandler(ModeloNotFoundException.class)
    public final ResponseEntity<Object> manejarModeloExcepciones(ModeloNotFoundException ex, WebRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
                request.getDescription(false));
        return new ResponseEntity<Object>(exceptionResponse, HttpStatus.NOT_FOUND);
    }

    /**
     * Función que valida cualquier error de argumentos en los servicios.
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errores = "";
        for (ObjectError e : ex.getBindingResult().getAllErrors()) {
            errores += e.getObjectName();
        }
        System.out.println("ERROR: "+errores);
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), "Validación fallida", errores);
        return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    /**
     * Función que valida los errores de tipo de argumentos en los servicios.
     */
    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
                request.getDescription(false));
        return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }
}
