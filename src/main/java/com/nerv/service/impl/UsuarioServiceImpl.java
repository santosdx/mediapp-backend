package com.nerv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nerv.dao.IUsuarioDAO;
import com.nerv.model.Usuario;
import com.nerv.service.intr.IUsuarioService;

/**
 * Clase se define como un Servicio y que implementa la interface
 * "IUsuarioService" donde se concreta la definición de las funciones de acceso a
 * datos para la entidad Usuario, mediante el uso de JPA.
 * 
 * @author santos
 *
 */
@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    private IUsuarioDAO daoUsuario;

    @Override
    public Usuario listarId(Integer id) {
        return daoUsuario.findOne(id);
    }

    @Override
    public List<Usuario> listar() {
        return daoUsuario.findAll();
    }

    @Override
    public Usuario registrar(Usuario t) {
        return daoUsuario.save(t);
    }

    @Override
    public Usuario modificar(Usuario t) {
        return daoUsuario.save(t);
    }

    @Override
    public void eliminar(Integer id) {
        daoUsuario.delete(id);
    }

}
