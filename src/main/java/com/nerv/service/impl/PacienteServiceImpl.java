package com.nerv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nerv.dao.IPacienteDAO;
import com.nerv.model.Paciente;
import com.nerv.service.intr.IPacienteService;

/**
 * Clase se define como un Servicio y que implementa la interface
 * "IPacienteService" donde se concreta la definición de las funciones de acceso
 * a datos para la entidad Paciente, mediante el uso de JPA.
 * 
 * @author santos
 *
 */
@Service
public class PacienteServiceImpl implements IPacienteService {

    @Autowired
    private IPacienteDAO daoPaciente;

    @Override
    public Paciente listarId(Integer id) {
        return daoPaciente.findOne(id);
    }

    @Override
    public List<Paciente> listar() {
        //return daoPaciente.findAll();
        return daoPaciente.findAllByOrderByNombresAsc();
    }

    @Override
    public Paciente registrar(Paciente t) {
        return daoPaciente.save(t);
    }

    @Override
    public Paciente modificar(Paciente t) {
        return daoPaciente.save(t);
    }

    @Override
    public void eliminar(Integer id) {
        daoPaciente.delete(id);
    }

}
