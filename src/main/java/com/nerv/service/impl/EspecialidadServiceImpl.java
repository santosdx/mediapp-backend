package com.nerv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nerv.dao.IEspecialidadDAO;
import com.nerv.model.Especialidad;
import com.nerv.service.intr.IEspecialidadService;

/**
 * Clase se define como un Servicio y que implementa la interface
 * "IEspecialidadService" donde se concreta la definición de las funciones de
 * acceso a datos para la entidad Especialidad, mediante el uso de JPA.
 * 
 * @author santos
 *
 */
@Service
public class EspecialidadServiceImpl implements IEspecialidadService {

    @Autowired
    private IEspecialidadDAO daoEspecialidad;

    @Override
    public Especialidad listarId(Integer id) {
        return daoEspecialidad.findOne(id);
    }

    @Override
    public List<Especialidad> listar() {
        return daoEspecialidad.findAll();
    }

    @Override
    public Especialidad registrar(Especialidad t) {
        return daoEspecialidad.save(t);
    }

    @Override
    public Especialidad modificar(Especialidad t) {
        return daoEspecialidad.save(t);
    }

    @Override
    public void eliminar(Integer id) {
        daoEspecialidad.delete(id);
    }

}
