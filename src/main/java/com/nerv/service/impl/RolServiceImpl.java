package com.nerv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nerv.dao.IRolDAO;
import com.nerv.model.Rol;
import com.nerv.service.intr.IRolService;

/**
 * Clase se define como un Servicio y que implementa la interface "IRolService"
 * donde se concreta la definición de las funciones de acceso a datos para la
 * entidad Rol, mediante el uso de JPA.
 * 
 * @author santos
 *
 */
@Service
public class RolServiceImpl implements IRolService {

    @Autowired
    private IRolDAO daoRol;

    @Override
    public Rol listarId(Integer id) {
        return daoRol.findOne(id);
    }

    @Override
    public List<Rol> listar() {
        return daoRol.findAll();
    }

    @Override
    public Rol registrar(Rol t) {
        return daoRol.save(t);
    }

    @Override
    public Rol modificar(Rol t) {
        return daoRol.save(t);
    }

    @Override
    public void eliminar(Integer id) {
        daoRol.delete(id);
    }

}
