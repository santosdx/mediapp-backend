package com.nerv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nerv.dao.IExamenDAO;
import com.nerv.model.Examen;
import com.nerv.service.intr.IExamenService;

/**
 * Clase se define como un Servicio y que implementa la interface
 * "IExamenService" donde se concreta la definición de las funciones de acceso a
 * datos para la entidad Examen, mediante el uso de JPA.
 * 
 * @author santos
 *
 */
@Service
public class ExamenServiceImpl implements IExamenService {

    @Autowired
    private IExamenDAO daoExamen;

    @Override
    public Examen listarId(Integer id) {
        return daoExamen.findOne(id);
    }

    @Override
    public List<Examen> listar() {
        return daoExamen.findAll();
    }

    @Override
    public Examen registrar(Examen t) {
        return daoExamen.save(t);
    }

    @Override
    public Examen modificar(Examen t) {
        return daoExamen.save(t);
    }

    @Override
    public void eliminar(Integer id) {
        daoExamen.delete(id);
    }

}
