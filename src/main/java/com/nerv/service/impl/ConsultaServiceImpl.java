package com.nerv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nerv.dao.IConsultaDAO;
import com.nerv.dao.IConsultaExamenDAO;
import com.nerv.dto.ConsultaListaExamenDTO;
import com.nerv.model.Consulta;
import com.nerv.service.intr.IConsultaService;

/**
 * Clase se define como un Servicio y que implementa la interface
 * "IConsultaService" donde se concreta la definición de las funciones de acceso
 * a datos para la entidad Consulta, mediante el uso de JPA.
 * 
 * @author santos
 *
 */
@Service
public class ConsultaServiceImpl implements IConsultaService {

    @Autowired
    private IConsultaDAO daoConsulta;
    @Autowired
    private IConsultaExamenDAO daoConsultaExamen;

    @Override
    public Consulta listarId(Integer id) {
        return daoConsulta.findOne(id);
    }

    @Override
    public List<Consulta> listar() {
        return daoConsulta.findAll();
    }

    @Override
    public Consulta registrar(Consulta t) {
        // Como a la columna (id_consulta) de la tabla (DetalleConsulta) se le agrego la
        // etiqueta (@JsonIgnore), la cual logra que en el Json de "Consulta" en el
        // atributo "detalleConsulta", que es un lista y que contiene una referencia a
        // consulta, no exista o se agrege la estructura Json de la referencia de
        // Consulta, para evitar generar un bucle infinito.

        // Por lo anterior, esta sección de codigo permite referenciar para cada objeto
        // Detalle al papa (Consulta), es decir agregar en el atributo (consulta) de
        // detalle, su refrencia a Consulta, la cual es el papa.

        t.getDetalleConsulta().forEach(detalle -> { // Recorremos el arreglo, expresión lamda.
            detalle.setConsulta(t); // Referenciamos al papa (t)
        });
        return daoConsulta.save(t);
    }

    @Transactional
    @Override
    public Consulta registrarMaestroDetalleMasExamenesTransaccional(ConsultaListaExamenDTO consultaDetalleExamenDTO) {
        consultaDetalleExamenDTO.getConsulta().getDetalleConsulta().forEach(detalle -> { // Recorremos el arreglo,
                                                                                         // expresión lamda.
            detalle.setConsulta(consultaDetalleExamenDTO.getConsulta()); // Referenciamos al papa
                                                                         // (consultaDetalleExamen.getConsulta())
        });
        // 1. Guardamos (insertamos) la parte de Consulta y Detalle.
        daoConsulta.save(consultaDetalleExamenDTO.getConsulta());
        // 2. Luego guardamos (insertamos cada examen)
        consultaDetalleExamenDTO.getListaExamen().forEach(examen -> {
            daoConsultaExamen.registrarNativeQuery(consultaDetalleExamenDTO.getConsulta().getIdConsulta(), examen.getIdExamen());
        });
        return consultaDetalleExamenDTO.getConsulta();
    }

    @Override
    public Consulta modificar(Consulta t) {
        return daoConsulta.save(t);
    }

    @Override
    public void eliminar(Integer id) {
        daoConsulta.delete(id);
    }

}
