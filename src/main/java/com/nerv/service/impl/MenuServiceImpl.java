package com.nerv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nerv.dao.IMenuDAO;
import com.nerv.model.Menu;
import com.nerv.service.intr.IMenuService;

/**
 * Clase se define como un Servicio y que implementa la interface
 * "IMenuService" donde se concreta la definición de las funciones de acceso a
 * datos para la entidad Menu, mediante el uso de JPA.
 * 
 * @author santos
 *
 */
@Service
public class MenuServiceImpl implements IMenuService {

    @Autowired
    private IMenuDAO daoMenu;

    @Override
    public Menu listarId(Integer id) {
        return daoMenu.findOne(id);
    }

    @Override
    public List<Menu> listar() {
        return daoMenu.findAll();
    }

    @Override
    public Menu registrar(Menu t) {
        return daoMenu.save(t);
    }

    @Override
    public Menu modificar(Menu t) {
        return daoMenu.save(t);
    }

    @Override
    public void eliminar(Integer id) {
        daoMenu.delete(id);
    }

}
