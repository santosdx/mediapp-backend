package com.nerv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nerv.dao.IMedicoDAO;
import com.nerv.model.Medico;
import com.nerv.service.intr.IMedicoService;

/**
 * Clase se define como un Servicio y que implementa la interface
 * "IMedicoService" donde se concreta la definición de las funciones de acceso a
 * datos para la entidad Medico, mediante el uso de JPA.
 * 
 * @author santos
 *
 */
@Service
public class MedicoServiceImpl implements IMedicoService {

    @Autowired
    private IMedicoDAO daoMedico;

    @Override
    public Medico listarId(Integer id) {
        return daoMedico.findOne(id);
    }

    @Override
    public List<Medico> listar() {
        return daoMedico.findAll();
    }

    @Override
    public Medico registrar(Medico t) {
        return daoMedico.save(t);
    }

    @Override
    public Medico modificar(Medico t) {
        return daoMedico.save(t);
    }

    @Override
    public void eliminar(Integer id) {
        daoMedico.delete(id);
    }

}
