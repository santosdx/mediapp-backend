package com.nerv.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nerv.dao.ISystemInfoDAO;
import com.nerv.model.SystemInfo;
import com.nerv.service.intr.ISystemInfoService;

/**
 * Clase se define como un Servicio y que implementa la interface
 * "ISystemInfoService" donde se concreta la definición de las funciones de
 * acceso a datos para la entidad SystemInfo, mediante el uso de JPA.
 * 
 * @author santos
 *
 */
@Service
public class SystemInfoServiceImpl implements ISystemInfoService {

    @Autowired
    private ISystemInfoDAO daoSystemInfo;

    @Override
    public SystemInfo listarId(Integer id) {
        return daoSystemInfo.findOne(id);
    }

    @Override
    public List<SystemInfo> listar() {
        return daoSystemInfo.findAll();
    }

    @Override
    public SystemInfo registrar(SystemInfo t) {
        return daoSystemInfo.save(t);
    }

    @Override
    public SystemInfo modificar(SystemInfo t) {
        return daoSystemInfo.save(t);
    }

    @Override
    public void eliminar(Integer id) {
        daoSystemInfo.delete(id);
    }

}
