package com.nerv.service.intr;

import com.nerv.model.Especialidad;

/**
 * Interface que extiende de ICRUD, para heredar las definiciones generales de
 * funciones de acceso a datos de la entidad Especialidad.
 * 
 * @author santos
 *
 */
public interface IEspecialidadService extends ICRUD<Especialidad> {

}
