package com.nerv.service.intr;

import com.nerv.model.SystemInfo;

/**
 * Interface que extiende de ICRUD, para heredar las definiciones generales de
 * funciones de acceso a datos de la entidad SystemInfo.
 * 
 * @author santos
 *
 */
public interface ISystemInfoService extends ICRUD<SystemInfo> {

}
