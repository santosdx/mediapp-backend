package com.nerv.service.intr;

import java.util.List;

/**
 * Interface que representa un CRUD, utiliza genericos <T> y en la que se
 * definen las funciones globales para el acceso y trabamiento de las entedidas
 * como tablas de base de datos.
 * 
 * @author santos
 *
 * @param <T>
 */
public interface ICRUD<T> {

    /**
     * Función que lista un objeto (registro) de la tabla de base de datos.
     * 
     * @param id Llave (código) del registro.
     * @return Objeto Registro de la tabla.
     */
    T listarId(Integer id);

    /**
     * Función que lista todos los objetos (registros) de la base de datos.
     * 
     * @return Lista Registros de la tabla.
     */
    List<T> listar();

    /**
     * Función que registra (inserta) un objeto (registro) en la tabla de base de
     * datos.
     * 
     * @param objeto Registro a insertar en la tabla.
     * @return objeto Registro insertado en la tabla.
     */
    T registrar(T objeto);

    /**
     * Función que modifica (actualiza) un objeto (registro) en la tabla de base de
     * datos.
     * 
     * @param objeto Registro actualizar en la tabla.
     * @return objeto Registro actualizado en la tabla.
     */
    T modificar(T objeto);

    /**
     * Función que permite eliminar (borrar) un objeto (registro) en la talba de
     * base de datos.
     * 
     * @param id Llave (código) del registro.
     */
    void eliminar(Integer id);
}
