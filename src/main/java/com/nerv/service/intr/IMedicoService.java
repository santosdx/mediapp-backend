package com.nerv.service.intr;

import com.nerv.model.Medico;

/**
 * Interface que extiende de ICRUD, para heredar las definiciones generales de
 * funciones de acceso a datos de la entidad Medico.
 * 
 * @author santos
 *
 */
public interface IMedicoService extends ICRUD<Medico> {

}
