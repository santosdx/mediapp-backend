package com.nerv.service.intr;

import com.nerv.model.Examen;

/**
 * Interface que extiende de ICRUD, para heredar las definiciones generales de
 * funciones de acceso a datos de la entidad Examen.
 * 
 * @author santos
 *
 */
public interface IExamenService extends ICRUD<Examen> {

}
