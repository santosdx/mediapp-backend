package com.nerv.service.intr;

import com.nerv.model.Menu;

/**
 * Interface que extiende de ICRUD, para heredar las definiciones generales de
 * funciones de acceso a datos de la entidad Menu.
 * 
 * @author santos
 *
 */
public interface IMenuService extends ICRUD<Menu> {

}
