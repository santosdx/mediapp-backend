package com.nerv.service.intr;

import com.nerv.model.Rol;

/**
 * Interface que extiende de ICRUD, para heredar las definiciones generales de
 * funciones de acceso a datos de la entidad Rol.
 * 
 * @author santos
 *
 */
public interface IRolService extends ICRUD<Rol> {

}
