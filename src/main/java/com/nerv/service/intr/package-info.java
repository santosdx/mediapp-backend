/**
 * Paquete que contiene el listado de interfaces que interactuan con los objetos de base de datos para definir los metodos de acceso a la base de datos, y que seran implementados por la capa de servicios.
 * (Select, Insert, Update, Delete, etc...)
 */
/**
 * @author santos
 *
 */
package com.nerv.service.intr;