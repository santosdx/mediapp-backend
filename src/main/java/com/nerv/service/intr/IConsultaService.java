package com.nerv.service.intr;

import com.nerv.dto.ConsultaListaExamenDTO;
import com.nerv.model.Consulta;

/**
 * Interface que extiende de ICRUD, para heredar las definiciones generales de
 * funciones de acceso a datos de la entidad Consulta.
 * 
 * @author santos
 *
 */
public interface IConsultaService extends ICRUD<Consulta> {

    /**
     * Función que registra (inserta) un objeto DTO, con la estructura Maestro
     * Detalle más la lista de Examenes. La función realiza la insercción en la
     * tabla, Consulta, DetalleConsulta y en la tabla ConsultaExamen. Todo lo
     * anterior se realiza de forma transaccional.
     * 
     * @param objetoDTO Registro a insertar en la tabla.
     * @return Consulta Registro insertado en la tabla.
     */
    Consulta registrarMaestroDetalleMasExamenesTransaccional(ConsultaListaExamenDTO consultaDetalleExamenDTO);
}
