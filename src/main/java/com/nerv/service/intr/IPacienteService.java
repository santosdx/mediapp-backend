package com.nerv.service.intr;

import com.nerv.model.Paciente;

/**
 * Interface que extiende de ICRUD, para heredar las definiciones generales de
 * funciones de acceso a datos de la entidad Paciente.
 * 
 * @author santos
 *
 */
public interface IPacienteService extends ICRUD<Paciente> {

}
