package com.nerv.service.intr;

import com.nerv.model.Usuario;

/**
 * Interface que extiende de ICRUD, para heredar las definiciones generales de
 * funciones de acceso a datos de la entidad Usuario.
 * 
 * @author santos
 *
 */
public interface IUsuarioService extends ICRUD<Usuario> {

}
