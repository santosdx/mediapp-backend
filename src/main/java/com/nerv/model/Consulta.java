package com.nerv.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa la tabla de base de datos "consulta", como un objeto
 * tipo (Entidad) en la plicación, con los atributos (columnas) de la tabla.
 * 
 * @author santos
 *
 */
@ApiModel(description = "Información de la Consulta")
@Entity
@Table(name = "consulta")
public class Consulta {

    @ApiModelProperty(notes = "Llave primaria de la tabla.")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idConsulta;

    @ApiModelProperty(notes = "Llave foranea de la tabla (paciente).")
    @ManyToOne
    @JoinColumn(name = "id_paciente", nullable = false)
    private Paciente paciente;

    @ApiModelProperty(notes = "Llave foranea de la tabla (medico).")
    @ManyToOne
    @JoinColumn(name = "id_medico", nullable = false)
    private Medico medico;

    @ApiModelProperty(notes = "Llave foranea de la tabla (especialidad).")
    @ManyToOne
    @JoinColumn(name = "id_especialidad", nullable = false)
    private Especialidad especialidad;

    @ApiModelProperty(notes = "Fecha de generación de la consulta.")
    @JsonSerialize(using = ToStringSerializer.class) // ISODate 2018-10-01T05:00:00.000 // Formato universal para
                                                     // fechas. //@JsonSerialize 'ermite convertir del Json, el string
                                                     // de date, a fecha date en Java
    private LocalDateTime fecha;

    @ApiModelProperty(notes = "Detalles de la consulta, de tipo Uno a Muchos. Carga perezosa (LAZY).")
    @OneToMany(mappedBy = "consulta", cascade = { CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.REMOVE }, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<DetalleConsulta> detalleConsulta;

    @ApiModelProperty(notes = "Examenes de la consulta, de tipo Uno a Muchos. Carga perezosa (LAZY).")
    @OneToMany(mappedBy = "consulta", cascade = { CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.REMOVE }, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<ConsultaExamen> consultaExamen;

    public Integer getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(Integer idConsulta) {
        this.idConsulta = idConsulta;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Especialidad getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad) {
        this.especialidad = especialidad;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public List<DetalleConsulta> getDetalleConsulta() {
        return detalleConsulta;
    }

    public void setDetalleConsulta(List<DetalleConsulta> detalleConsulta) {
        this.detalleConsulta = detalleConsulta;
    }

    public List<ConsultaExamen> getConsultaExamen() {
        return consultaExamen;
    }

    public void setConsultaExamen(List<ConsultaExamen> consultaExamen) {
        this.consultaExamen = consultaExamen;
    }

    @Override
    public String toString() {
        return "Consulta [idConsulta=" + idConsulta + ", paciente=" + paciente + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idConsulta == null) ? 0 : idConsulta.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Consulta other = (Consulta) obj;
        if (idConsulta == null) {
            if (other.idConsulta != null)
                return false;
        } else if (!idConsulta.equals(other.idConsulta))
            return false;
        return true;
    }

}
