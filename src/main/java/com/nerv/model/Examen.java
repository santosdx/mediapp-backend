package com.nerv.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa la tabla de base de datos "examen", como un objeto tipo
 * (Entidad) en la plicación, con los atributos (columnas) de la tabla.
 * 
 * @author santos
 *
 */
@ApiModel(description = "Información de Examen")
@Entity
@Table(name = "examen")
public class Examen {

    @ApiModelProperty(notes = "Llave primaria de la tabla.")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idExamen;

    @ApiModelProperty(notes = "Nombre debe tener minimo 3 caracteres y maximo 50.")
    @Size(min = 3, max = 50, message = "Nombre debe tener minimo 3 caracteres y maximo 50.")
    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @ApiModelProperty(notes = "Descripción debe tener minimo 3 caracteres y maximo 250.")
    @Size(min = 3, max = 250, message = "Descripción debe tener minimo 3 caracteres y maximo 250.")
    @Column(name = "descripcion", nullable = false, length = 250)
    private String descripcion;

    public Integer getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(int idExamen) {
        this.idExamen = idExamen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Examen [idExamen=" + idExamen + ", nombre=" + nombre + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idExamen == null) ? 0 : idExamen.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Examen other = (Examen) obj;
        if (idExamen == null) {
            if (other.idExamen != null)
                return false;
        } else if (!idExamen.equals(other.idExamen))
            return false;
        return true;
    }

}
