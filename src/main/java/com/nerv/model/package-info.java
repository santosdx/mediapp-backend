/**
 * Paquete que contiene el listado de clases (Entity) que representan una entidad (tabla) de base de datos, como un mosdelo de objetos en la aplicación.
 */
/**
 * @author santos
 *
 */
package com.nerv.model;