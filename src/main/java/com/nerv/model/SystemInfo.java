package com.nerv.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Clase que representa la tabla de base de datos "system_info", como un objeto
 * tipo (Entidad) en la plicación, con los atributos (columnas) de la tabla.
 * 
 * @author santos
 *
 */
@Entity
@Table(name = "system_info")
public class SystemInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSystemInfo;

    @Column(name = "nombre_app", nullable = false, length = 100)
    private String nombreApp;

    public Integer getIdSystemInfo() {
        return idSystemInfo;
    }

    public void setIdSystemInfo(Integer idSystemInfo) {
        this.idSystemInfo = idSystemInfo;
    }

    public String getNombreApp() {
        return nombreApp;
    }

    public void setNombreApp(String nombreApp) {
        this.nombreApp = nombreApp;
    }

    @Override
    public String toString() {
        return "SystemInfo [idSystemInfo=" + idSystemInfo + ", nombreApp=" + nombreApp + "]";
    }

}
