package com.nerv.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa la tabla de base de datos "usuario", como un objeto tipo
 * (Entidad) en la plicación, con los atributos (columnas) de la tabla.
 * 
 * @author santos
 *
 */
@ApiModel(description = "Información del Usuario")
@Entity
@Table(name = "usuario")
public class Usuario {

    @ApiModelProperty(notes = "Llave primaria de la tabla.")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idUsuario;

    @ApiModelProperty(notes = "Nickame debe tener minimo 6 caracteres y maximo 12.")
    @Size(min = 6, max = 12, message = "Nickame debe tener minimo 6 caracteres y maximo 12.")
    @Column(name = "nickname", nullable = false, length = 12)
    private String nickname;

    @ApiModelProperty(notes = "Password debe tener minimo 3 caracteres y maximo 12.")
    @Size(min = 3, max = 12, message = "password debe tener minimo 3 caracteres y maximo 12.")
    @Column(name = "password", nullable = false, length = 12)
    private String password;

    @ApiModelProperty(notes = "Nombres debe tener minimo 3 caracteres y maximo 70.")
    @Size(min = 3, max = 70, message = "Nombre debe tener minimo 3 caracteres y maximo 70.")
    @Column(name = "nombres", nullable = false, length = 70)
    private String nombres;

    // @Column(name="estado", nullable=false, length=70)
    // private String estado;

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @Override
    public String toString() {
        return "Usuario [idUsuario=" + idUsuario + ", nickname=" + nickname + "]";
    }

}
