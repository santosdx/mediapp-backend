package com.nerv.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa la tabla de base de datos "medico", como un objeto tipo
 * (Entidad) en la plicación, con los atributos (columnas) de la tabla.
 * 
 * @author santos
 *
 */
@ApiModel(description = "Información del Medico")
@Entity
@Table(name = "medico")
public class Medico {

    @ApiModelProperty(notes = "Llave primaria de la tabla.")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMedico;

    @ApiModelProperty(notes = "Nombres debe tener minimo 3 caracteres y maximo 70.")
    @Size(min = 3, max = 70, message = "Nombres debe tener minimo 3 caracteres y maximo 70.")
    @Column(name = "nombres", nullable = false, length = 70)
    private String nombres;

    @ApiModelProperty(notes = "Apellidos debe tener minimo 3 caracteres y maximo 70.")
    @Size(min = 3, max = 70, message = "Apellido debe tener minimo 3 caracteres y maximo 70.")
    @Column(name = "apellidos", nullable = false, length = 70)
    private String apellidos;

    @ApiModelProperty(notes = "CMP debe tener 12 caracteres.")
    @Size(min = 12, max = 12, message = "CMP debe tener 12 caracteres.")
    @Column(name = "cmp", nullable = false, length = 12)
    private String cmp;

    public Integer getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Integer idMedico) {
        this.idMedico = idMedico;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCmp() {
        return cmp;
    }

    public void setCmp(String cmp) {
        this.cmp = cmp;
    }

    @Override
    public String toString() {
        return "Medico [idMedico=" + idMedico + ", nombres=" + nombres + "]";
    }

}
