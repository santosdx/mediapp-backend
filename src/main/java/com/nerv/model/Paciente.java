package com.nerv.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa la tabla de base de datos "paciente", como un objeto
 * tipo (Entidad) en la plicación, con los atributos (columnas) de la tabla.
 * 
 * @author santos
 *
 */
@ApiModel(description = "Información del Paciente")
@Entity
@Table(name = "paciente")
public class Paciente {

    @ApiModelProperty(notes = "Llave primaria de la tabla.")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPaciente;

    @ApiModelProperty(notes = "Nombres debe tener minimo 3 caracteres y maximo 70.")
    @Size(min = 3, max = 70, message = "Nombres debe tener minimo 3 caracteres y maximo 70.")
    @Column(name = "nombres", nullable = false, length = 70)
    private String nombres;

    @ApiModelProperty(notes = "Apellidos debe tener minimo 3 caracteresy maximo 70.")
    @Size(min = 3, max = 70, message = "Apellidos debe tener minimo 3 caracteres y maximo 70.")
    @Column(name = "apellidos", nullable = false, length = 70)
    private String apellidos;

    @ApiModelProperty(notes = "DNI debe tener minimo 6 caracteres y maximo 12.")
    @Size(min = 6, max = 12, message = "DNI debe tener minimo 6 caracteres y maximo 12")
    @Column(name = "dni", nullable = false, length = 12)
    private String dni;

    @ApiModelProperty(notes = "Dirección debe tener minimo 3 y maximo 150 caracteres.")
    @Size(min = 3, max = 150, message = "Dirección debe tener minimo 3 y maximo 150 caracteres.")
    @Column(name = "direccion", nullable = false, length = 150)
    private String direccion;

    @ApiModelProperty(notes = "Telefono debe tener minimo 7 caracteres y maximo 10.")
    @Size(min = 7, max = 10, message = "Telefono debe tener minimo 7 caracteres y maximo 10.")
    @Column(name = "telefono", nullable = false, length = 10)
    private String telefono;

    @ApiModelProperty(notes = "Correo electronico del paciente.")
    @Column(name = "email", nullable = false, length = 55)
    private String email;

    public Integer getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Paciente [idPaciente=" + idPaciente + ", nombres=" + nombres + "]";
    }

}
