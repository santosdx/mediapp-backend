package com.nerv.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa la tabla de base de datos "menu", como un objeto tipo
 * (Entidad) en la plicación, con los atributos (columnas) de la tabla.
 * 
 * @author santos
 *
 */
@ApiModel(description = "Información de Menu")
@Entity
@Table(name = "menu")
public class Menu {

    @ApiModelProperty(notes = "Llave primaria de la tabla.")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMenu;

    @ApiModelProperty(notes = "Nombre debe tener minimo 3 caracteres y maximo 100.")
    @Size(min = 3, max = 100, message = "Nombre debe tener minimo 3 caracteres y maximo 100.")
    @Column(name = "nombre", nullable = false, length = 100)
    private String nombre;

    @ApiModelProperty(notes = "Url debe tener minimo 3 caracteres y maximo 150.")
    @Size(min = 3, max = 100, message = "Url debe tener minimo 3 caracteres y maximo 150.")
    @Column(name = "url", nullable = false, length = 150)
    private String url;

    @ApiModelProperty(notes = "Icono debe tener minimo 3 caracteres y maximo 50.")
    @Size(min = 3, max = 50, message = "Icono debe tener minimo 3 caracteres y maximo 50.")
    @Column(name = "icono", nullable = true, length = 50)
    private String icono;

    public Integer getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(Integer idMenu) {
        this.idMenu = idMenu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    @Override
    public String toString() {
        return "Menu [idmenu=" + idMenu + ", nombre=" + nombre + "]";
    }

}
