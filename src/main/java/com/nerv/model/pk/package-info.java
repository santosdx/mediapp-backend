/**
 * Paquete que contiene el listado de clases que definen los objetos que representan las llaves de base de datos.
 */
/**
 * @author santos
 *
 */
package com.nerv.model.pk;