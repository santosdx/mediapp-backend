package com.nerv.model.pk;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.nerv.model.Consulta;
import com.nerv.model.Examen;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que define la llave compuesta para la tala "consulta_examen".
 * 
 * @author santos
 *
 */
@ApiModel(description = "Llave compuesta de la tabla (consulta_examen9")
@Embeddable // Permite que la case sea embebida en otra.
public class ConsultaExamenPK implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "Llave foranea de la tabla examen.")
    @ManyToOne
    @JoinColumn(name = "idExamen", nullable = false)
    private Examen examen;

    @ApiModelProperty(notes = "Llave foranea de la tabla consulta.")
    @ManyToOne
    @JoinColumn(name = "idConsulta", nullable = false)
    private Consulta consulta;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((consulta == null) ? 0 : consulta.hashCode());
        result = prime * result + ((examen == null) ? 0 : examen.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConsultaExamenPK other = (ConsultaExamenPK) obj;
        if (consulta == null) {
            if (other.consulta != null)
                return false;
        } else if (!consulta.equals(other.consulta))
            return false;
        if (examen == null) {
            if (other.examen != null)
                return false;
        } else if (!examen.equals(other.examen))
            return false;
        return true;
    }

}
