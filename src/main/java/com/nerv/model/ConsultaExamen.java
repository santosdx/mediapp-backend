package com.nerv.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.nerv.model.pk.ConsultaExamenPK;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa la tabla de base de datos "consulta_examen", como un
 * objeto tipo (Entidad) en la plicación, con los atributos (columnas) de la
 * tabla.
 * 
 * @author santos
 *
 */
@ApiModel(description = "Información de la relación Consulta Examen")
@Entity
@IdClass(ConsultaExamenPK.class)
@Table(name = "consulta_examen")
public class ConsultaExamen {

    @ApiModelProperty(notes = "Llave primaria compuesta de la tabla.")
    @Id
    private Consulta consulta;

    @ApiModelProperty(notes = "Llave primaria compuesta de la tabla.")
    @Id
    private Examen examen;

    public Consulta getConsulta() {
        return consulta;
    }

    public void setConsulta(Consulta consulta) {
        this.consulta = consulta;
    }

    public Examen getExamen() {
        return examen;
    }

    public void setExamen(Examen examen) {
        this.examen = examen;
    }

    @Override
    public String toString() {
        return "ConsultaExamen [consulta=" + consulta.getIdConsulta() + ", examen=" + examen.getIdExamen() + "]";
    }

}
