package com.nerv.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa la tabla de base de datos "detalle_consulta", como un
 * objeto tipo (Entidad) en la plicación, con los atributos (columnas) de la
 * tabla.
 * 
 * @author santos
 *
 */
@ApiModel(description = "Información de Detalle de la Consulta")
@Entity
@Table(name = "detalle_consulta")
public class DetalleConsulta {

    @ApiModelProperty(notes = "Llave primaria de la tabla.")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDetalle;

    @ApiModelProperty(notes = "Llave foranea de la tabla (consulta).")
    @JsonIgnore // Esta anotación permite ignorar esta referencia dentro del Json, con la
                // finalizar de generar un ucle infinito en el Json general de Maestro Detalle
    @ManyToOne
    @JoinColumn(name = "id_consulta", nullable = false)
    private Consulta consulta;

    @ApiModelProperty(notes = "Diagnostico debe tener minimo 3 caracteres y maximo 70.")
    @Size(min = 3, max = 70, message = "Diagnostico debe tener minimo 3 caracteres y maximo 70.")
    @Column(name = "diagnostico", nullable = false, length = 70)
    private String diagnostico;

    @ApiModelProperty(notes = "Tratamiento debe tener minimo 3 caracteres y maximo 300.")
    @Size(min = 3, max = 70, message = "Tratamiento debe tener minimo 3 caracteres y maximo 300.")
    @Column(name = "tratamiento", nullable = false, length = 300)
    private String tratamiento;

    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(int idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Consulta getConsulta() {
        return consulta;
    }

    public void setConsulta(Consulta consulta) {
        this.consulta = consulta;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    @Override
    public String toString() {
        return "DetalleConsulta [idDetalle=" + idDetalle + ", consulta=" + consulta.getIdConsulta() + "]";
    }

}
