package com.nerv.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Clase que representa la tabla de base de datos "especialidad", como un objeto
 * tipo (Entidad) en la plicación, con los atributos (columnas) de la tabla.
 * 
 * @author santos
 *
 */
@ApiModel(description = "Información de la Especialidad")
@Entity
@Table(name = "especialidad")
public class Especialidad {

    @ApiModelProperty(notes = "Llave primaria de la tabla.")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idEspecialidad;

    @ApiModelProperty(notes = "Nombre debe tener minimo 3 caracteres y maximo 50.")
    @Size(min = 3, max = 50, message = "Nombre debe tener minimo 3 caracteres y maximo 50.")
    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    public Integer getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Integer idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Especialidad [idEspecialidad=" + idEspecialidad + ", nombre=" + nombre + "]";
    }

}
